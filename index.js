const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();

const app = express();

process.env.PORT = process.env.PORT || 3000;



// CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    next();
});

// ---------------    Midlewares ------------------------
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


//-----------------    Routes  --------------------------
app.use(require('./src/routes/index.routes'));


// ------------- Starting Server -------------------------
app.listen(process.env.PORT, () => {
    console.log('Servidor iniciado en puerto:', process.env.PORT);
})


//-------------- Conection to databse --------------------
mongoose.connection.openUri( process.env.DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(res => console.log('Conexion DB establecida'))
    .catch(err => console.log(err))
