const mongoose = require('mongoose');
const schema = mongoose.Schema;

const matriculaSchema = new schema({
    cliente : { type: schema.Types.ObjectId, ref: 'Cliente', required: [true, 'El usuario es nesesario'] },
    plan: { type: schema.Types.ObjectId, ref: 'Plan', required: [true, 'El nombre del plan es nesesario'] },
    fecha: { type: Date, required: [true, 'La fecha del registro de la matricula es nesesaria'] },
    fechaInicio: { type: Date },
    fechaFin: { type: Date },
    total: { type: Number, required: [true, 'El monto final es nesesario'] },
    cupon: { type: schema.Types.ObjectId, ref: 'Cupon' },
    estado: { type: String },
    pago: { type: Number, required: [true, 'El monto pagado es nesesario'] },
    estadoPago: { type: Boolean, required: [true, 'El estado de pago es nesesario '] }
});

module.exports = mongoose.model('Matricula', matriculaSchema);
