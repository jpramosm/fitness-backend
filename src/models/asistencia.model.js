const mongoose = require('mongoose');
const schema = mongoose.Schema;

const asistenciaSchema = new schema({
    cliente: { type: schema.Types.ObjectId, ref: 'Cliente', required: [true, 'El usuario es nesesario'] },
    fecha: { type: Date, required: [true, 'La fecha es nesesaria'] },
    empleado :{ type: schema.Types.ObjectId, ref: 'Empleado', required: [true, 'El empleado es nesesario'] }
})


module.exports = mongoose.model('Asistencia',asistenciaSchema);