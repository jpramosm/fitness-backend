const mongoose = require('mongoose');
const schema = mongoose.Schema;

const planSchema = new schema({
    cantidadMeses: { type: Number, required: [true, 'La cantidad de meses es nesesaria'] },
    precio: { type: Number, required: [true, 'El precio del plan es obligatorio'] },
    descripcion: { type: String, required: [true, 'Debe incuir una breve descripcion del plan'] },
    imagen: { type: String, required: [true, 'La imagen del plan es nesesaria '] }
});

module.exports = mongoose.model('Plan',planSchema);