const mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

let usuarioSchema = new Schema({
    
    nombre: { type: String, required: [true, 'El nombre es nesesario'] },
    apellidos: { type: String, required: [true, 'Los apellidos son nesesarios'] },
    imagen : {type: String , default:'https://res.cloudinary.com/continental/image/upload/v1575729276/steve-transparent-profile-12_bsmcna.png' },
    correo: { type: String , unique : true },
    password: { type: String },
    estado: { type: String },
    dni: { type: String },
    telefono: { type: String },
    nacimiento: { type: Date },
    genero: { type: Number },
    tipoRegistro :  { type : String }

});

usuarioSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Cliente', usuarioSchema );
