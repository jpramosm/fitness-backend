const mongoose = require('mongoose');
const schema = mongoose.Schema;

const cuponSchema = new schema({
    nombre: { type: String, required: [true, 'El nombre de la promocion es nesesario'] },
    descuento: { type: Number, required: [true, 'el porcentaje de desceuento es nesesario'] },
    estado: { type: Boolean, default: true }
});

module.exports = mongoose.model('Cupon', cuponSchema);
