const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let empleadoSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es nesesario'] },
    apellidos: { type: String, required: [true, 'Los apellidos son nesesarios'] },
    dni: { type: String, required: [true, 'El numero de documento de identidad es nesesario '] },
    telefono: { type: String, required: [true, 'El numero de telefono es nesessario'] },
    correo: { type: String, required: [true, 'La direccion de correo electronico es nesesaria'] },
    password: { type: String, required: [true, 'La contraaseña es nesesaria'] },
    direccion: { type: String, required: [true, 'La direccion actual de domicilio es nesesaria'] },
    cargo: { type: String, required: [true, 'El cargo que ocupa es nesesario'], default: 'Vendedor' },
    estado: { type: Boolean, default: true },
    imagen : {type : String , default : 'https://res.cloudinary.com/continental/image/upload/v1579809724/user_no_photo_300x300_tua2yq.png' }
});

module.exports = mongoose.model('Empleado', empleadoSchema);
