const express = require('express');
const Plan = require('../models/plan.models');
const router = express.Router();

//================ Registrar nuevo plan ===============

router.post('/plan', async (req, res) => {
    const contenido = req.body;

    const plan = new Plan({
        cantidadMeses: contenido.cantidadMeses,
        precio: contenido.precio,
        descripcion: contenido.descripcion,
        imagen : contenido.imagen
    });

    try {
        const resultado = await plan.save();
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }
});

//================ Obtener todos los planes ======================

router.get('/plan', async (rq, res) => {
    try {
        const resultado = await Plan.find();
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }
});

//================ Obtener un solo plan ========================

router.get('/plan/:id', async (req, res) => {
    const id = req.params.id;

    try {
        const resultado = await Plan.findById(id);
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }

});

//================ Actualizar plan ===================

router.put('/plan', async (req, res) => {
    const contenido = req.body;

    try {
        const resultado = await Plan.findOneAndUpdate({ _id: contenido._id }, contenido, { new: true });
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }
});

module.exports = router;