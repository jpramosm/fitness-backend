const express = require('express');
const randomstring = require('randomstring');
const bcrypt = require('bcryptjs');
const path = require('path');
const multer = require('multer');
const cloudinary = require('cloudinary').v2;
const fs = require('fs');
const Cliente = require('../models/cliente.model');


const correo = require('../config/correo/verificacionCorreo');

const router = express.Router();


cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET
});


const storage = multer.diskStorage({
    destination : path.resolve(__dirname,`../uploads/user`),
    filename:(req, file, cb)=>{
        cb(null,Date.now() + path.extname(file.originalname))
    }
});

const upload = multer({
    storage,
}) .single('image');



//============ Creacion de nuevo usuario (cliente) ========================

router.post('/cliente', async (req, res) => {
    const contenido = req.body;

    const cliente = new Cliente({
        nombre: contenido.nombre,
        apellidos: contenido.apellidos,
        dni : contenido.dni,
        correo: contenido.correo,
        estado : contenido.estado,
        password: bcrypt.hashSync(contenido.password),
        tipoRegistro : 'Normal'
    });

    if (contenido.nacimiento) {
        cliente.nacimiento = contenido.nacimiento
    }

    
    if (contenido.telefono) {
        cliente.telefono = contenido.telefono
    }

    try {
        const resultado = await cliente.save();
        
        if(resultado.estado === 'RegistradoAdmin'){
            
            return res.status(200).json(resultado);
        }
        correo.verificarEmail(resultado.correo,{nombre:resultado.nombre,codigo:resultado._id});
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }

});

// ================== Listar todos los clientes =================

router.get('/cliente', async (req, res) => {
    
    try {
        const resultado = await Cliente.find();
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }

});


// ============= actualizar datos del usuaario ===================

router.put('/cliente/:id', async (req, res) => {

    const contenido = req.body;
    const id = req.params.id;

    if(contenido.password){
        contenido.password =  bcrypt.hashSync(contenido.password)
    }

    try {
        const resultado = await Cliente.findOneAndUpdate({ _id: id }, contenido , { new: true });
        res.status(200).json(resultado);
        if (contenido.correo) {
            correo.verificarEmail(contenido.correo,{nombre:resultado.nombre,codigo:contenido._id}); 
        }
    } catch (error) {
        res.status(500).json(error);
    }

});



//================= Buscar un solo usuario ========================

router.get('/cliente/:id', async(req, res) => {
    const id = req.params.id;

    try {
        const resultado = await Cliente.findById(id);
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }

});


//========================= Login Normal ================================

router.post('/cliente/login', (req, res) => {
    const contenido = req.body;
    Cliente.findOne({ correo: contenido.correo }, (err, data) => {
        if (err) {
            res.status(500).json(err);
        } else {
            if(data){

                if (data.tipoRegistro !== 'Normal') {
                    return res.status(500).json('utlize el login por goolgle');
                }

                if ( bcrypt.compareSync(contenido.password,data.password) ) {
                    data.password = '******************'
                    res.status(200).json(data);
                } else {
                    res.status(500).json('Contraseña incorrecta');
                }
            }else{
                res.status(500).json('El correo no existe');
            }
        };
    });
});


// =================== Login redes Sociales =======================

router.post('/cliente/loginsocial', (req, res) => {
    
    const contenido = req.body;

    const cliente = new Cliente({
        nombre: contenido.nombre,
        apellidos: contenido.apellidos,
        imagen: contenido.imagen,
        estado : 'Confirmado',
        correo: contenido.correo,
        tipoRegistro: contenido.tipoRegistro
    });



    Cliente.findOne({ correo: contenido.correo }, (err, data) => {
        if (err) {
            res.status(500).json(err);
        } else {
            if (data) {
                res.status(200).json(data)
            } else {
                cliente.save((err2, data2) => {
                    if (err2) {
                        res.status(500).json(err2);
                    } else {
                        res.status(200).json(data2);
                    }
                });
            }
        };
    });
});



// ===========  Guardar, subir foto de producto a cloudinary  ======================

router.put('/cliente/imagen/:id', upload, async (req, res) => {

    const id = req.params.id;
    const file = req.file;

    try {
        const resultadoSubida = await cloudinary.uploader.upload(file.path);
        
        Cliente.findOneAndUpdate({_id:id},{imagen:resultadoSubida.secure_url},{new :true},(err,data)=>{
            if (err) {
                res.status(500).json(err);
            }else{
                 fs.unlink(file.path,(err)=>{
                    if (err) {
                        console.log(err);
                    }else{
                        console.log('borrado');
                    }
                });
                res.status(200).json(data);
            }
        });
        
    } catch (error) {
        res.status(500).json(error);
    }
}); 

// ========================== Actualizar correo ==============================

router.put('/cliente/correo/:id', async (req, res) => {
    const id = req.params.id;
    const contenido = req.body;

    contenido.password = bcrypt.hashSync(contenido.password);

    try {
        const resultado = await Cliente.findOneAndUpdate({ _id: id }, contenido, { new: true });
        res.status(200).json(resultado);
        correo.verificarEmail(contenido.correo, { nombre: resultado.nombre, codigo: resultado._id });
    } catch (error) {
        res.status(500).json(error);
    }
});

// ======================= enviar coreo de verificacion ==================

router.put('/cliente/enviarcorreo/correo',async (req,res)=>{
    const contenido = req.body;
   

        const resultado = await correo.verificarEmail(contenido.correo,{nombre:contenido.nombre,codigo:contenido._id});
        return res.send(resultado);
 
});



router.get('/cliente/buscar/:busqueda', (req, res) => {

    let letra = req.params.busqueda;
    let regex = new RegExp(letra, 'i');

    Cliente.find()
        .or([{nombre:regex},{apellidos:regex}])
        .exec((err, data) => {
            if (err) {
                return res.status(500).json(err);
            } else {
                res.status(200).json(data);
            };
        })
    
});






module.exports = router;