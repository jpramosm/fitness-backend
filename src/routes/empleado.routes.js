const express = require('express');
const bcrypt = require('bcryptjs');
const Empleado = require('../models/empleado.model'); 
const path = require('path');
const multer = require('multer');
const cloudinary = require('cloudinary').v2;
const fs = require('fs');
const  router = express.Router();

cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET
});


const storage = multer.diskStorage({
    destination : path.resolve(__dirname,`../uploads/user`),
    filename:(req, file, cb)=>{
        cb(null,Date.now() + path.extname(file.originalname))
    }
});

const upload = multer({
    storage,
}) .single('image');


//================= Registrar nuevo empleado ==============

router.post('/empleado', async (req, res) => {
    const contenido = req.body;

    const empleado = new Empleado({
        nombre: contenido.nombre,
        apellidos: contenido.apellidos,
        dni: contenido.dni,
        telefono: contenido.telefono,
        correo: contenido.correo,
        password: bcrypt.hashSync(contenido.password),
        direccion: contenido.direccion,
        cargo: contenido.cargo
    });

    try {
        const resultado = await empleado.save();
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }


});


// ======================== login Empleado ================================

router.post('/empleado/login', (req, res) => {
    const contenido = req.body;

    Empleado.findOne({ dni: contenido.dni }, (err, data) => {
        if (err) {
            res.status(500).json(err);
        } else {
            if(data){
                if ( bcrypt.compareSync(contenido.password,data.password) ) {
                    data.password = '******************'
                    res.status(200).json(data);
                } else {
                    res.status(500).json('Contraseña incorrecta');
                }
            }else{
                res.status(500).json('El dni no existe');
            }
        };
    });
});


//======================= Listar todos los empleados =============================

router.get('/empleado', async (req, res) => {

    try {
        const resultado = await Empleado.find();
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }

});


//======================== Obtener datos de un solo empleado ================================

router.get('/empleado/:empleado', async (req, res) => {

    const empleado = req.params.empleado

    try {
        const resultado = await Empleado.findById(empleado);
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }

});

//============================ Actualizar empleado ====================

router.put('/empleado/:empleado', async (req, res) => {

    const contenido = req.body;
    const empleado = req.params.empleado;

    try {
        const resultado = await Empleado.findOneAndUpdate({ _id: empleado }, contenido, { new: true });
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }

});

//===================== Cambiar Foto perfil empleado ==============================

router.put('/empleado/imagen/:id', upload, async (req, res) => {

    const id = req.params.id;
    const file = req.file;

    try {
        const resultadoSubida = await cloudinary.uploader.upload(file.path);
        
        Empleado.findOneAndUpdate({_id:id},{imagen:resultadoSubida.secure_url},{new :true},(err,data)=>{
            if (err) {
                res.status(500).json(err);
            }else{
                 fs.unlink(file.path,(err)=>{
                    if (err) {
                        console.log(err);
                    }else{
                        console.log('borrado');
                    }
                });
                res.status(200).json(data);
            }
        });
        
    } catch (error) {
        res.status(500).json(error);
    }
}); 


module.exports = router;