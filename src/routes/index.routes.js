const express = require('express');
const app = express();

app.use(require('./cliente.routes'));
app.use(require('./plan.routes'));
app.use(require('./cupon.routes'));
app.use(require('./matricula.routes'));
app.use(require('./empleado.routes'));
app.use(require('./asistencia.routes'));
app.use(require('./conteo.routes'))

module.exports = app;