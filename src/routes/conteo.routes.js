const express = require('express');
const router = express.Router();
const cliente = require('../models/cliente.model');
const matricula = require('../models/matricula.models');
const empleado = require('../models/empleado.model');


router.get('/conteo/cliente',async (req,res)=>{
    try {
        const resultado = await cliente.count();
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }
});

router.get('/conteo/empleados',async (req,res)=>{
    try {
        const resultado = await empleado.count();
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }
});

//987

router.get('/conteo/matricula', async (req, res) => {
    try {
        const resultado = await matricula.count().or([{ estado: 'iniciado' }, { estado: 'no iniciado' }]);
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }
});

module.exports = router;
