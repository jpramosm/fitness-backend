const express = require('express');
const Asistencia = require('../models/asistencia.model');
const router = express.Router();


// =================== Registrar asistencia ========================

router.post('/asistencia', async (req, res) => {
    const contenido = req.body;

    const fecha = new Date();
    fecha.setHours(fecha.getHours() - 5);

    const asisitencia = new Asistencia({
        cliente: contenido.cliente._id,
        fecha: fecha,
        empleado: contenido.empleado._id,
    });

    try {
        const resultado = await asisitencia.save();
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }

});


// ===================== ruta para confirmar asistencia del cliente ===========================


router.post('/asistencia/confirmar',async (req,res)=>{
    
    const contenido = req.body;

    const dat = new Date(req.body.fecha);
    const date = new Date(req.body.fecha)
    const datef = new Date(date.setDate(date.getDate() + 1));

    Asistencia.findOne().and([{ fecha: { $gte: dat, $lt: datef } },{cliente:contenido._id}]).exec((err, data) => {
        if (err) {
            return res.status(500).json(err);
        } else {
            res.status(200).json(data);
        };
    });
});


// ====================== Listar todos las asistencias ===========================


router.get('/asistencia', async (req, res) => {

    try {
        const resultado = await Asistencia.find().populate('cliente empleado');
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }

});


//================== Listar asistencias por cliente ==========================


router.get('/asistencia/cliente/:cliente', (req, res) => {

    const cliente = req.params.cliente;

    
     Asistencia.find({cliente : cliente }).populate('cliente empleado').exec((err, data) => {
        if (err) {
            return res.status(500).json(err);
        } else {
            res.status(200).json(data);
        };
    })
  

});


//===================== Listar asistencias por fecha ==========================

router.get('/asistencia/fecha/:fecha', async (req, res) => {

    const fecha = req.params.fecha;

    const dat = new Date(fecha);
    const date = new Date(fecha)
    const datef = new Date(date.setDate(date.getDate() + 1));

    try {
        const resultado = await Asistencia.find({ fecha: { $gte: dat, $lt: datef } }).populate('cliente empleado');
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }

});


//====================== Listar asistencias de cliente por matricula ====================

router.get('/asistencia/cliente/:fecha1/:fecha2/:cliente', async (req, res) => {

    const fecha1 = req.params.fecha1;
    const fecha2 = req.params.fecha2;
    const cliente = req.params.cliente;

    const dateIni = new Date(fecha1);
    const dateEnd = new Date(fecha2);

    dateIni.setHours(dateIni.getHours() - 5);

    try {
        const resultado = await Asistencia.find({ fecha: { $gte: dateIni, $lt: dateEnd },cliente :cliente  }).populate('cliente empleado');
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error)
    }

});






module.exports = router;