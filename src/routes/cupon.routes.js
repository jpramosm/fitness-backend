const express = require('express');
const Cupon = require('../models/cupon.model');
const router = express.Router();

//============== Registrar cupon ===============

router.post('/cupon', async (req, res) => {
    const contenido = req.body;

    const cupon = new Cupon({
        nombre: contenido.nombre ,
        descuento: contenido.descuento
    });

    try {
        const resultado = await cupon.save();
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }

})


//=============== Listar todos lo cupones ================

router.get('/cupon', async (req, res) => {
    try {
        const resultado = await Cupon.find();
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }
});


//=============== Obtener un solo cupon ===============

router.get('/cupon/:nombre', async (req, res) => {
    const nombre = req.params.nombre
    try {
        const resultado = await Cupon.findOne({ nombre: nombre });
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }
});



//================ Actualizar datos cupon =================

router.put('/cupon/:id', async (req, res) => {
    const contenido = req.body;
    const id = req.params.id;

    console.log('asddddd');

    try {
        const resultado = await Cupon.findOneAndUpdate({_id:id},contenido,{new:true} );
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }

})


module.exports = router;
