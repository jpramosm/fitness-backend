const express = require('express');
const randomstring = require('randomstring');
const Matricula = require('../models/matricula.models');
const correo = require('../config/correo/verificacionCorreo');
const router = express.Router();


router.post('/matricula', async  (req, res) => {
    const contenido = req.body;

    console.log(contenido);

    const fecha = new Date();
    fecha.setHours(fecha.getHours() - 5);

    const matricula = new Matricula({
        cliente: contenido.cliente._id,
        plan: contenido.plan._id,
        fecha: fecha, 
        fechaInicio: contenido.fechaInicio,
        fechaFin: contenido.fechaFin,
        total: contenido.total,
        estado: 'no iniciado',
        pago: contenido.pago,
        estadoPago: contenido.estadoPago
    });

    if (contenido.cupon) {
        matricula.cupon = contenido.cupon._id; 
    }

    try {
        const resultado = await matricula.save();
        let dataCorreo = {
            nombre: contenido.cliente.nombre,
            apellidos: contenido.cliente.apellidos,
            fecha: contenido.fecha,
            meses: contenido.plan.cantidadMeses,
            total: contenido.total,
            pago: contenido.estadoPago
        }
        correo.matricula(contenido.cliente.correo, dataCorreo)
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }
});


router.put('/matricula', async  (req, res) => {
    
    const contenido = req.body;

    const fecha = new Date();
    fecha.setHours(fecha.getHours() - 5);
    
    if(contenido.fechaInicio){
        contenido.fechaInicio = fecha
    }

    try {
        const resultado = await Matricula.findByIdAndUpdate(contenido._id ,contenido);
         res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }
});

router.get('/matricula', async (req, res) => {
    const id = req.params.id;
    try {
        const resultado = await Matricula.find().populate('cupon plan cliente')
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }
});

router.get('/matricula/estado', async (req, res) => {

    try {
        const resultado = await Matricula.find().or([{estado:'no iniciado'},{estado:'iniciado'}]).populate('cupon plan cliente')
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }
});


router.get('/matricula/:id', async (req, res) => {
    const id = req.params.id;
    try {
        const resultado = await Matricula.findById(id).populate('cupon plan cliente')
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }
});

router.get('/matricula/cliente/:id', async (req, res) => {
    const id = req.params.id;
    try {
        const resultado = await Matricula.find({ cliente: id }).populate('cupon plan cliente')
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }
});


router.get('/matricula/fecha/:fecha', (req, res) => {
    const dat = new Date(req.params.fecha);
    const date = new Date(req.params.fecha)
    const datef = new Date(date.setDate(date.getDate() + 1));


    Matricula.find({ fecha: { $gte: dat, $lt: datef } }).populate('cliente').exec((err, data) => {
        if (err) {
            return res.status(500).json(err);
        } else {
            res.status(200).json(data);
        };
    })

});


router.get('/matricula/pago/:estado', async  (req, res) => {

    const estado = req.params.estado;
    try {
        const resultado = await Matricula.find({ estadoPago: estado }).populate('cupon plan cliente')
        res.status(200).json(resultado);
    } catch (error) {
        res.status(500).json(error);
    }
})

module.exports = router;