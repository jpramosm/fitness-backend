const nodemailer = require('nodemailer');
const path = require('path');
const ejs = require('ejs');

let correo = {};

const transporteer = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'energymhuancayo@gmail.com',
        pass: 'energym123'
    }, tls: {
        rejectUnauthorized: false
    }
});

let enviarmensaje = (para, asunto, contenido, next) => {
    let correoOpciones = {
        from: 'energymhuancayo@gmail.com',
        to: para,
        subject: asunto,
        html: contenido
    };
    transporteer.sendMail(correoOpciones, next);
}


/// ===================== Funcion verificar email ===========================

correo.verificarEmail = async (para, data) => {

    const pathTemplate = path.resolve(__dirname, './correo.ejs');
    const template = await ejs.renderFile(pathTemplate, { nombre: data.nombre, codigo: `http//:localhost:4200/verificar/${data.codigo}` });

    enviarmensaje(para, 'Verificación de correo Electronico', template, (err, resp) => {
        if (err) {
            console.log(err);
            return
        } else {
            console.log('Correo Enviado');
        }
    });
}


//========================== Enviar correo de matricula ===============================

correo.matricula = async(para, data)=>{

    let  contenido = {};
    
    contenido.usuario = `${data.nombre}`;
    contenido.fecha = new Date(contenido.fecha);
    contenido.meses = data.meses;
    contenido.total = data.total;
    contenido.pago = data.pago;

    const pathTemplate = path.resolve(__dirname, './matricula.ejs');
    const template = await ejs.renderFile( pathTemplate, contenido );

    enviarmensaje(para, 'Verificación de correo Electronico', template , (err, resp) => {
        if (err) {
            console.log(err);
            return
        } else {
            console.log('Correo Enviado');
        }
    });

};

module.exports = correo;